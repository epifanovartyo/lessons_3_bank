﻿using System;
using System.Collections.Generic;

namespace Lesson_3
{
    class Client
    {
        public string name { get; set; }
        public string surname;
        public int age;
        public double balance;

        public void ReName(string name)
        {
            this.name = name;
        }
        public void ReAge(int age)
        {
            this.age = age;
        }
        public void ReSurName(string surname)
        {
            this.surname = surname;
        }
        public void ReBalance(double balance)
        {
            this.balance = balance;
        }
        public Client(string name, string surname, int age, double balance)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.balance = balance;
        }
    }

    class Bank
    {
        public string NameBank;
        public string AdressBank;
        List<Client> clients = new List<Client>();

        public Bank(string NameBank, string AdressBank)
        {
            this.NameBank = NameBank;
            this.AdressBank = AdressBank;
        }
        public void AddClient(string name, string surname, int age, double balance)
        {
            Client client = new Client(name, surname, age, balance);
            clients.Add(client);
        }
        public void ShowClient()
        {
            string SearchName = Console.ReadLine();

            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine("Имя клиента: " + clients[i].name);
                    Console.WriteLine("Фамилия клиента: " + clients[i].surname);
                    Console.WriteLine("Возраст клиента: " + clients[i].age);
                    Console.WriteLine("Баланс клиента: " + clients[i].balance);
                    Console.WriteLine("====================");
                }
            }
        }
        public void RemoveClient()
        {
            string SearchName = Console.ReadLine();

            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    clients.Remove(clients[i]);
                }
            }
        }
        public void ReClient()
        {
            
            string SearchName = Console.ReadLine();

            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine("Имя клиента: " + clients[i].name);
                    Console.WriteLine("Фамилия клиента: " + clients[i].surname);
                    Console.WriteLine("Возраст клиента: " + clients[i].age);
                    Console.WriteLine("Баланс клиента: " + clients[i].balance);
                    Console.WriteLine("====================");

                    Console.WriteLine("Выберите изменение:\n" + "1.Изменить имя клиента.\n" + "2.Изменить фамилию клиента.\n" + "3.Изменить возраст клиента.\n" + "4.Изменить баланс клиента.\n");
                    int choice = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("\n");

                    if (choice == 1)
                    {
                        Console.WriteLine("Введите новое имя: ");
                        string name = Console.ReadLine();
                        clients[i].ReName(name);
                    }

                    if (choice == 2)
                    {
                        Console.WriteLine("Введите новую фамилию: ");
                        string surname = Console.ReadLine();
                        clients[i].ReSurName(surname);
                    }

                    if (choice == 3)
                    {
                        Console.WriteLine("Введите новый возраст: ");
                        int age = Convert.ToInt32(Console.ReadLine());
                        clients[i].ReAge(age);
                    }

                    if (choice == 4)
                    {
                        Console.WriteLine("Введите новый баланс: ");
                        double balance = Convert.ToDouble(Console.ReadLine());
                        clients[i].ReBalance(balance);
                    }
                }
            }
        }

        public void ShowAllClient()
        {
            clients.Sort((left, right) => left.name.CompareTo(right.name));
            foreach (Client client in clients)
            {
                Console.WriteLine("====================");
                Console.WriteLine("Имя клиента: " + client.name);
                Console.WriteLine("Фамилия клиента: " + client.surname);
                Console.WriteLine("Возраст клиента: " + client.age);
                Console.WriteLine("Баланс клиента: " + client.balance);
                Console.WriteLine("====================");
                
            }
        }
    }

    class ClassMain
    {
        public void MainClass()
        {
            Bank bank = new Bank("Банк будущего!!!", "ул.Та самая д.Вечный");   
            bank.AddClient("Игорь", "Бурый", 76, 90000);
            bank.AddClient("Валера", "Бурый", 76, 90000);
            bank.AddClient("Георгий", "Бурый", 76, 90000);
            Console.WriteLine(bank.NameBank);
            Console.WriteLine(bank.AdressBank);
            Console.WriteLine("=====================");
            

            while (true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("Выберите операцию банка:\n" + "1.Добавить клиента.\n" + "2.Удалить клиента.\n" + "3.Изменение информации клиента.\n" + "4.Информация о клиенте.\n" + "5.Информация обо всех клиентах банка.\n");
                int Choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\n");

                if (Choice == 1)
                {
                    Console.WriteLine("Введите имя клиента:");
                    string _name = Console.ReadLine();
                    Console.WriteLine("Введите фамилию клиента:");
                    string _surname = Console.ReadLine();
                    Console.WriteLine("Введите возраст клиента:");
                    int _age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите баланс клиента:");
                    double _balance = Convert.ToDouble(Console.ReadLine());
                    bank.AddClient(_name, _surname, _age, _balance);

                    Console.WriteLine("Клиенты успешно создан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 2)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.RemoveClient();

                    Console.WriteLine("Клиент успешно удалён, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 3)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ReClient();

                    Console.WriteLine("Имя успешно изменено, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 4)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ShowClient();

                    Console.WriteLine("Клиент успешно показан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 5)
                {
                    bank.ShowAllClient();

                    Console.WriteLine("Клиенты успешно показаны, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }     
            } 
        }

        public static void Main(string[] args)
        {
            ClassMain classMain = new ClassMain();
            classMain.MainClass();
        }
    }
}
